import { Component } from '@angular/core';

import { PortfolioPage } from '../portfolio/portfolio';
import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { MorePage } from '../more/more';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  // this tells the tabs component which Pages
  // should be each tab's root Page
  tab1Root: any = AboutPage;
  tab2Root: any = PortfolioPage;
  tab3Root: any = MorePage;
  tab4Root: any = ContactPage;

  constructor() {

  }
}
